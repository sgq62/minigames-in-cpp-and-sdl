﻿#pragma once

#include "screen.h"

class IScene
{
public:


	
	virtual ~IScene() = default;
	virtual void InitializeScene(Screen *screen) =0;
	virtual void HandleInput()=0;
	virtual void Update(const int deltaTime)=0;
	virtual void Draw()=0;
	virtual void CloseScene()=0;
	//virtual Scenes GetSceneID() =0;
};
