﻿#pragma once

#include <SDL.h>
#include <string>

class LTexture
{
    public:

		static inline SDL_Renderer * renderer = nullptr;
        //Initializes variables
        LTexture();

        //Deallocates memory
        ~LTexture();

        //Loads image at specified path
        bool LoadFromFile( std::string path );

        //Deallocates texture
        void Free();

        //Set color modulation
        void SetColor( Uint8 red, Uint8 green, Uint8 blue );

        //Set blending
        void SetBlendMode( SDL_BlendMode blending );

        //Set alpha modulation
        void SetAlpha( Uint8 alpha );
        
        //Renders texture at given point
        void Render( int x, int y, SDL_Rect* clip = nullptr, double angle = 0.0, SDL_Point* center = nullptr, SDL_RendererFlip flip = SDL_FLIP_NONE );

        //Gets image dimensions
        int GetWidth();
        int GetHeight();

    private:
        //The actual hardware texture
        SDL_Texture* texture;

        //Image dimensions
        int width;
        int height;
};
