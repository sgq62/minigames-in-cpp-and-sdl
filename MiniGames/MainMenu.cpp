﻿#include "MainMenu.h"
#include "inputManager.h"
#include "SceneFactory.h"
#include <iostream>

MainMenu::~MainMenu()
{
	//Test
	std::cout << "Mina Menu Gone \n";
}

void MainMenu::InitializeScene(Screen *screen)
{
	this->screen = screen;
	this->screen->SetWindowTitle("Main Menu!");
	mainMenuLogo.LoadFromFile("images/mainMenu.png");
}

void MainMenu::HandleInput()
{
	if(Input::IsPressed(Input::Keys::DOWN))
	{
		SceneFactory::GetInstance()->SwapScenes(Scenes::PONG,*screen);
	}
}

void MainMenu::Update(const int deltaTime)
{
}

void MainMenu::Draw()
{
	mainMenuLogo.Render((screen->GetWidth() - mainMenuLogo.GetWidth()) /2,(screen->GetHeight() - mainMenuLogo.GetHeight()) /2);
}

void MainMenu::CloseScene()
{
	mainMenuLogo.Free();
	std::cout << "Closing Main Menu\n";
}

//IScene::Scenes MainMenu::GetSceneID()
//{
//	return Scenes::MAIN_MENU;
//}
