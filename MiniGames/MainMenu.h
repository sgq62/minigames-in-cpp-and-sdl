﻿#pragma once

#include "IScene.h"
#include "LTexture.h"

class MainMenu : public IScene
{
public:
	~MainMenu();
	void InitializeScene(Screen* screen) override;
	void HandleInput() override;
	void Update(const int deltaTime) override;
	void Draw() override;
	void CloseScene() override;
	//IScene::Scenes GetSceneID() override;
private:
	Screen* screen;
	LTexture mainMenuLogo{};
};
