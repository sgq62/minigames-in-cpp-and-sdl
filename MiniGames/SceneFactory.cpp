﻿#include "SceneFactory.h"

IScene* SceneFactory::CreateScene(const Scenes scene)
{
	switch (scene) {
		case Scenes::MAIN_MENU:
			return new MainMenu{};
			break;
		case Scenes::PONG:
			return new Pong{};
			break;
		default:
			return nullptr;
	}
}

void SceneFactory::SwapScenes(Scenes sceneToSwapTo, Screen& screen)
{
	if (currentSceneID == sceneToSwapTo)
	{
		return;
	}
	currentSceneID = sceneToSwapTo;
	if (currentScene)
	{
		currentScene->CloseScene();
		delete currentScene; 
		currentScene = SceneFactory::CreateScene(sceneToSwapTo);
	}
	else
	{
		currentScene = SceneFactory::CreateScene(sceneToSwapTo);
	}
	currentScene->InitializeScene(&screen);
}
