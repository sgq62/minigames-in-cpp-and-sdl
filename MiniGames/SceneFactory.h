﻿#pragma once

#include "MainMenu.h"
#include "pong.h"
#include "screen.h"

enum class Scenes
{
	NONE,
	MAIN_MENU,
	PONG
};

class SceneFactory
{
private:
	static inline SceneFactory *ptrInstance;
	SceneFactory() = default;
	IScene* CreateScene(const Scenes scene);
	Scenes currentSceneID{Scenes::NONE};
public:
	IScene* currentScene = nullptr;
	
	static inline SceneFactory *GetInstance()
	{
		if(!ptrInstance)
		{
			ptrInstance = new SceneFactory;
		}
		return ptrInstance;
	}
	void SwapScenes(Scenes sceneToSwapTo, Screen& screen);
};
