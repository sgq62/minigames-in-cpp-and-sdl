﻿#include "inputManager.h"

bool Input::IsPressed(const Keys key)
{
	const Uint8* currentKeyStates = SDL_GetKeyboardState(nullptr);
	return currentKeyStates[static_cast<SDL_Scancode>(key)];
}
