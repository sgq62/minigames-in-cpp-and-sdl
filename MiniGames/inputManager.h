﻿#pragma once
#include <SDL.h>




class Input
{
public:

	enum class Keys : int
	{
		UP = SDL_SCANCODE_UP,
		DOWN = SDL_SCANCODE_DOWN
	};
	
	Input() = delete;
	~Input() = delete;
	static bool IsPressed(const Keys key);
private:
	
};