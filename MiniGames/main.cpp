#define SDL_MAIN_HANDLED
#include "screen.h"
#include "inputManager.h"
#include "SceneFactory.h"

#include <iostream>



int main()
{
	Screen gameScreen{};
	gameScreen.Initialize(1280, 720);
	bool quit = false;
	SDL_Event event;
	SceneFactory* sceneFactoryInst = SceneFactory::GetInstance();
	
	sceneFactoryInst->SwapScenes(Scenes::MAIN_MENU,gameScreen);

	/*SceneFactory::currentScene = SceneFactory::CreateScene(SceneFactory::Scenes::MAIN_MENU);
	SceneFactory::currentScene->InitializeScene(gameScreen);*/

	while (!quit)
	{
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
			{
				quit = true;
			}

			sceneFactoryInst->currentScene->HandleInput();
		}

		sceneFactoryInst->currentScene->Update(1.0/60.0);
		gameScreen.ClearScreen();
		sceneFactoryInst->currentScene->Draw();
		gameScreen.RenderScreen();
	}

	return 0;
}
