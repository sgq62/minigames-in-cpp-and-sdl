﻿#include "pong.h"

void Pong::InitializeScene(Screen *screen)
{
	this->screen = screen;
	this->screen->SetWindowTitle("Pong!!");
}

void Pong::HandleInput()
{
}

void Pong::Update(const int deltaTime)
{
}

void Pong::Draw()
{
}

void Pong::CloseScene()
{
}

//IScene::Scenes Pong::GetSceneID()
//{
//	return Scenes::PONG;
//}
