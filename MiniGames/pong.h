﻿#pragma once

#include "IScene.h"

class Pong : public IScene
{
public:
	void InitializeScene(Screen *screen) override;
	void HandleInput() override;
	void Update(const int deltaTime) override;
	void Draw() override;
	void CloseScene() override;
	//IScene::Scenes GetSceneID() override;
private:
	Screen *screen;
};
