﻿#include "screen.h"
#include <string>
#include <SDL_image.h>
#include "LTexture.h"

Screen::~Screen()
{
	Close();
}

bool Screen::Initialize(const int screenWidth,const int screenHeight)
{
	bool success = true;
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		success = false;
	} else
	{
		window = SDL_CreateWindow("Minigames!",SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, screenWidth, screenHeight, SDL_WINDOW_SHOWN );
		if(window)
		{
			height = screenHeight;
			width = screenWidth;
			renderer = SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED);
			if(renderer)
			{
				SDL_SetRenderDrawColor(renderer,0xFF,0xFF,0xFF,0xFF);
				const int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					success = false;
				} else
				{
					LTexture::renderer = renderer;
				}
			}else
			{
				success = false;
			}
		} else
		{
			success = false;
		}
	}
	return success;
}

void Screen::SetWindowTitle(const char* sceneTitle)
{
	std::string title{"Mini Games - "};
	title += sceneTitle;

	
	SDL_SetWindowTitle(window, title.c_str());
}

void Screen::ClearScreen()
{
	SDL_RenderClear(renderer);
}

void Screen::RenderScreen()
{
	SDL_RenderPresent(renderer);
}

void Screen::Close()
{
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	window = nullptr;
	renderer = nullptr;
	IMG_Quit();
	SDL_Quit();
}

int Screen::GetWidth() const
{
	return width;
}

int Screen::GetHeight() const
{
	return height;
}
