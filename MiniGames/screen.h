#pragma once

#include <SDL.h>


class Screen
{
public:
	Screen() = default;
	~Screen();
	bool Initialize(const int screenWidth,const int screenHeight);
	void SetWindowTitle(const char* sceneTitle);
	void ClearScreen();
	void RenderScreen();
	void Close();
	int GetWidth() const;
	int GetHeight() const;
private:
	SDL_Window* window = nullptr;
	SDL_Surface* screenSurface = nullptr;
	SDL_Renderer* renderer = nullptr;
	int height{};
	int width{};
};